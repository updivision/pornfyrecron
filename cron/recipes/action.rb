cron "minute" do
  minute "*"
  hour "*"
  day "*"
  weekday "*"
  month "*"
  command "wget -O - -q -t 1 https://sharesome.com/scheduled-tasks > /dev/null"
end
